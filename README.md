# Introdução 
API para cadastro e consulta de pedidos 

# Requisição
> Homologacao: http://35.247.213.54/api/client/

> Producao: http://api.carriers.com.br/client/

```JSON
 curl -X GET \
   http://35.247.213.54/api/client/ \
  -H 'Authorization: Bearer API-KEY' \
  -H 'cache-control: no-cache'
  ```

* Cadastro de pedidos:
    * [Cadastro de pedidos](ConsultaPedidos/CadastroPedidos.md)

* Consulta Tracking:
    * [Consulta de Tracking](ConsultaPedidos/ConsultaTracking.md)
    * [Consulta de Tracking V2](ConsultaPedidos/ConsultaTrackingV2.md)

* Consulta Frete:
    * [Consulta de Frete](ConsultaPedidos/ConsultaFrete.md)
