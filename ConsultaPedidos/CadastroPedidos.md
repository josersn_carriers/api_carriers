# Introdução
A API para cadastrar novos pedidos


### Solicitação POST
> /Carriers/newOrder

# Parâmetros
|Campo|Descricao|Obrigatório|Tipo             
|----------------|---------------|---------------|---------------|
Pedidos|Dados dos pedidos|sim|Objeto **Pedidos**
PackList|Sequencia numérica para identifica pedidos da requisição|sim|Numeric
IE|Inscrição estadual|sim|Numeric
CNPJ|Cnpj do cliente|sim|Numeric
Origem|Origem da requisição|sim|String
 
### Objeto **Pedidos**
|Campo|Descricao|Obrigatório|Tipo             
|----------------|---------------|---------------|---------------|
TipoEntrega|1=ENTREGA<br>2=RETIRADA<br>3=EDROP|sim|Numeric
IdDropPoint|Se TipoEntrega=3 esse campo deve ser informado|nao|Numeric
Destinatario|Nome do Destinatario do pedido|sim|String
Endereco|Endereco de entrega|sim|String
Numero|Numero do endereço de entrega|sim|String
Complemento|Complemento do endereço de entrega|não|String
Referencia|Referencia do endereço de entrega |não|String
Bairro|Bairro do endereço de entrega|sim|String
Cidade|Cidade do endereço de entrega|sim|String
UF|Sigla do estado do endereço de entrega|sim|String
CEP|Cep do endereço de entrega|sim|Numeric
Documento|Documento do destinatário|sim|Numeric
IE|Inscrição estadual do destinatário|não|Numeric
PedidoCliente|Chave identificadora do pedido do cliente |sim|String
Email|E-mail de contato do destinatário|não|String
Telefone|telefone de contato do destinatário|não|Numeric
SerieNF|Serie da nota fiscal|sim|Numeric
NotaFiscal|Numero da nota fiscal|sim|Numeric
dtEmissaoNF|Data de emissão da nota|sim|Date
ValorNF|Valor da nota fiscal|sim|Numeric
ChaveNF|Chave da nota fiscal|sim|Numeric
NaturezaMercadoria|Natureza da mercadoria|não|String
OutrosDados|informações adicionais|não|String
Produtos|Descrição dos produtos|sim|Objeto **Produtos**
Volumes|Dados dos volumes|sim|Objeto **Volumes**

### Objeto **Produtos**
|Campo|Descricao|Obrigatório|Tipo             
|----------------|---------------|---------------|---------------|
Descricao|Descricao dos produtos|não|String

### Objeto **Volumes**
|Campo|Descricao|Obrigatório|Tipo             
|----------------|---------------|---------------|---------------|
CodigoBarras|Codigo de barras do volume|sim|String
Peso|Peso do volume|sim|Numeric (Kg)
Altura|Altura do volume|sim|Numeric (cm)
Largura|Largura do volume|sim|Numeric (cm)
Comprimento|Comprimento do volume|sim|Numeric (cm)
Cubagem|Cubagem do volume|sim|Numeric (M³)
Pesocub_Cli|Peso cubado do volume|Nao|Numeric (Kg)

## Exemplo de requisição

```JSON
curl -X POST \
  {Host}/Carriers/newOrder \
  -H 'Authorization: Bearer API_KEY' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
    "PackList": "04172019",
    "IE": "298206458118",
    "CNPJ": "45543915059232",
    "Origem": "EMBU DAS ARTES",
    "Pedidos": [
        {
            "TipoEntrega": "1",
            "Destinatario": "João Destinatário",
            "Endereco": "Rua Martinopolis",
            "Numero": "132",
            "Complemento": "Apto 255",
            "Referencia": "Próximo a estação CPTM Vila Leopoldina",
            "Bairro": "jd Ampermarg",
            "Cidade": "carapicuiba",
            "UF": "SP",
            "CEP": "06385843",
            "Documento": "17329479020",
            "PedidoCliente": "54JJD",
            "SerieNF": "9",
            "NotaFiscal": "12345678",
            "dtEmissaoNF": "03/11/2019",
            "ValorNF": 42,
            "ChaveNF": "12345678901234567890123456789012345678901234",
            "Produtos": [
                 {
                    "Descricao": "Bermuda SKU Variação 1 Azul"
                }
            ],
            "Volumes": [
                {
                    "CodigoBarras": "C@451234567812S",
                    "Peso": 1,
                    "Altura": 10,
                    "Largura": 20,
                    "Comprimento": 30,
                    "Cubagem":0.01,
                    "Pesocub_Cli":1.67
                },
                {
                    "CodigoBarras": "C@451234567822S",
                    "Peso": 1,
                    "Altura": 50,
                    "Largura": 60,
                    "Comprimento":70,
                    "Cubagem": 0.01,
                    "Pesocub_Cli":1.67
                }
            ]
        }
    ]
}'
```

### Códigos de erro 

Erros de sintaxe ou inesperados (erro 500)
```JS
{
    "status": "erro",
    "errors": {
        "servidor": [
            "Erro no servidor"
        ]
    }
}
```

Erros de requisição (erro 400)
```JS
{
    "status": "erro",
    "errors": {
        "requisicao": [
            "nenhum dado informado"
        ]
    }
}
```



## Resposta
|Campo|Descricao|            
|----------------|---------------|
JaCadastrados|Dados de pedidos já cadastrados |
NaoCadastrado|Dados de pedidos não cadastrados |
Cadastrado|Dados de pedidos cadastrados 
ProblemasRequisicao|Dados de pedidos com problemas na requisição 

```JS
{
    "JaCadastrados": [
        {
            "PedidoCliente": "54JJDDDD5DD",
            "Destinatario": "JOAO TESTE 2",
            "CodigoRastreio": "853N-54JJDDDD5DD-06385843",
            "Url": "http://201.39.92.60/portal/localizador.php?l=853N-54JJDDDD5DD-06385843"
        }
    ],
    "NaoCadastrado": [
     {
            "PedidoCliente": "tttt",
            "Destinatario": "JOAO DESTINATARIO"
        }],
    "Cadastrado": [
        {
            "IdPedido": "221665",
            "PedidoCliente": "54JJDDDD5rrrDD",
            "Destinatario": "JOAO TESTE 2",
            "CodigoRastreio": "853N-54JJDDDD5rrrDD-06385843",
            "Url": "http://201.39.92.60/portal/localizador.php?l=853N-54JJDDDD5rrrDD-06385843"
        }
    ],
    "ProblemasRequisicao": [
        {
            "PedidoCliente": "54JJDRRRRR",
            "erros": [
                "O campo (bairro) é requerido.",
                "O campo (cidade) é requerido.",
                "O campo (u f) é requerido.",
                "O campo (c e p) é requerido."
            ]
        }
    ]
}
```
